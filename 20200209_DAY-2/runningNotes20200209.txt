Agenda:

- Account Creation with AWS 
    - Root User Login
    - IAM User Login 
        Types of Logins:
        
            - Mgmt Console (Browser)
        Programmatic Access:    
            - AWS CLI (aws cli software)
            - SDK (Programming Language)

1. Networking and Content Delivery - VPC (Region) 

2. Compute - EC2 (Region)